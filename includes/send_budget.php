<?php

header('Access-Control-Allow-Origin: *');  

if($_POST)
{
	$name         = $_POST['nome'];
	$email        = $_POST['email'];
	$servicos     = $_POST['servico'];
	$telefone     = $_POST['telefone'];
	$message      = $_POST['msg'];

	$to          = 'contato@drsampaioneto.com.br';
	$from        = 'contato@drsampaioneto.com.br';
	$cc        	 = 'ogilvieira@gmail.com';
	$subject     = 'Mensagem do site [DÚVIDA]';
	$reply       = $email;

    if(PHP_OS == "Linux"){
    
        $quebra_linha = "\n";	//SLinux / Unix-like
    
    }
    elseif(PHP_OS == "WINNT"){
    
        $quebra_linha = "\r\n"; 	// Windows --'
    
    }else{
    
        echo "Este script nao esta preparado para funcionar com o sistema operacional de seu servidor";
    
    }

	$headers  = "MIME-Version: 1.1" . $quebra_linha . "";
	$headers .= "Content-type: text/html; charset=utf-8" . $quebra_linha . ""; 
	$headers .= "From: ".$from. $quebra_linha . "";
	$headers .= "Cc: ".$cc. $quebra_linha . "";
	$headers .= "Return-Path: " .$reply. $quebra_linha . "";

	$msg = "
		Nome: ".$name."<br>
		Email: ".$email."<br>
		Telefone: ".$telefone."<br>
		<br>";

	foreach($servicos as $servico) {
	    $msg .= '- '.$servico.', <br>';
	}

	$msg .= "Mensagem: ".$message;

	if(mail($to, $subject, $msg, $headers, '-r '.$from))
	{
		$arr_response = array(
			'send'	=> true
		);
	} else {
		$arr_response = array(
			'send'	=> false
		);
	}

	echo json_encode($arr_response);
}
?>