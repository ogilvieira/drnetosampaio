<?php

    $base_url = 'http://192.168.1.5/drnetosampaio/';
    // $base_url = 'http://www.drsampaioneto.com.br/site/';

    if(!$title){ $title = 'Dermatologia & Estética Médica'; }
    if(!$description){ $description = 'Dermatologia & Estética Médica no Jardim Paulista, São Paulo.'; }    
    if(!$keywords){ $keywords = 'peeling, estetica, dermatologia, dermatologista, esteticista, botox, cristal, marcar, consulta, jardim, paulista, são, paulo '; }
?>

<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Dr. José Sampaio Lopes Neto<?php if($title){ echo " | ".$title; } ?></title>

        <!-- for Google -->
        <meta name="description" content="<?php echo $description; ?>" />
        <meta name="keywords" content="<?php echo $keywords; ?>" />

        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

        <!-- for Google plus -->
        <meta itemprop="name" content="<?php echo $title; ?>">
        <meta itemprop="description" content="<?php echo $description; ?>">
        <meta itemprop="image" content="<?php echo $base_url; ?>social-preview.png">


        <!-- for Facebook -->          
        <meta property="og:title" content="<?php echo $title; ?>" />
        <meta property="og:site_name" content="<?php echo $title; ?>"/>
        <meta property="og:type" content="article" />
        <meta property="og:image" content="<?php echo $base_url; ?>social-preview.png" />
        <meta property="og:url" content="<?php echo $base_url; ?>" />
        <meta property="og:description" content="<?php echo $description; ?>" />

        <!-- for Twitter -->          
        <meta name="twitter:card" content="summary_large_image">
        <!-- <meta name="twitter:site" content="@"> -->
        <meta name="twitter:title" content="<?php echo $title; ?>">
        <meta name="twitter:description" content="<?php echo $description; ?>">
        <meta name="twitter:image:src" content="<?php echo $base_url; ?>social-preview.png">

        <!-- favicons -->
        <link rel="shortcut icon" href="<?php echo $base_url; ?>favicon.png">
        <link rel="apple-touch-icon" href="<?php echo $base_url; ?>apple-touch-icon-precomposed.png">

        <!-- fonts -->
        <link href='//fonts.googleapis.com/css?family=Ubuntu:300,400,500,700|Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>

        <!-- stylesheets -->
        <link rel="stylesheet" href="<?php echo $base_url; ?>assets/css/vendor/normalize.min.css">
        <link rel="stylesheet" href="<?php echo $base_url; ?>assets/css/vendor/grid-1200-responsive.css">
        <link rel="stylesheet" href="<?php echo $base_url; ?>assets/css/vendor/owl.carousel.css">
        <link rel="stylesheet" href="<?php echo $base_url; ?>assets/css/vendor/owl.transitions.css">
        <link rel="stylesheet" href="<?php echo $base_url; ?>assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo $base_url; ?>assets/css/main.css">

        <script src="<?php echo $base_url; ?>assets/js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
    <body>
    <header class="main-header">
        <div class="container-12">
            <div class="grid-12">
                <div class="logo">
                    <a href="<?php echo $base_url; ?>">Dr. Sampaio Neto</a>
                </div>
                <nav class="main-nav">
                    <ul>
                        <li><a <?php if($menu_marker == "home"){ echo "class=\"is-active\""; } ?>href="<?php echo $base_url; ?>">Home</a></li>
                        <li><a <?php if($menu_marker == "sobre"){ echo "class=\"is-active\""; } ?>href="<?php echo $base_url; ?>sobre">Sobre</a></li>
                        <li><a <?php if($menu_marker == "servicos"){ echo "class=\"is-active\""; } ?>href="<?php echo $base_url; ?>servicos">Serviços</a></li>
                        <li><a <?php if($menu_marker == "contato"){ echo "class=\"is-active\""; } ?>href="<?php echo $base_url; ?>contato">Contato</a></li>
                    </ul>
                </nav>
                <button class="btn-menu">
                    <i class="fa fa-bars"></i>
                </button>
            </div>
        </div>
    </header>

    <div class="fixed-menu">
        <div class="content">
            <nav class="main-nav-fixed">
                <ul>
                    <li><a href="<?php echo $base_url; ?>" class="home is-active"><span class="icon"></span> <span class="label">Home</span></a></li>
                    <li><a href="<?php echo $base_url; ?>/sobre" class="sobre "><span class="icon"></span> <span class="label">Sobre</span></a></li>
                    <li><a href="<?php echo $base_url; ?>/servicos" class="servicos "><span class="icon"></span> <span class="label">Serviços</span></a></li>
                    <li><a href="<?php echo $base_url; ?>/contato" class="contato "><span class="icon"></span> <span class="label">Contato</span></a></li>
                </ul>
                <a href="#" class="btn-fb">
                    <i class="fa fa-facebook"></i>
                    <span class="label">Curta nossa página</span>
                </a>
            </nav>
        </div>
    </div>
    <div class="fix-header"></div>