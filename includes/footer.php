    <footer class="main-footer">
        <div class="container-12">
            <div class="grid-6">
                <div class="copyright">
                    <span class="icon"></span>
                    <span class="label">
                        <span class="green-mark">Dr. José Sampaio Lopes Neto</span> | Dermatologia & Estética Médica
                    </span>
                </div>
            </div>
            <div class="grid-6">
                <div class="menu-footer">
                    <a href="<?php echo $base_url; ?>">Home</a> | <a href="<?php echo $base_url; ?>sobre">Sobre</a> | <a href="<?php echo $base_url; ?>servicos">Serviços</a> | <a href="<?php echo $base_url; ?>contato">Contato</a>
                </div>
            </div>
        </div>
    </footer>



        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script>
            
            var utils = {
                urlsite : '<?php echo $base_url; ?>'
            };

            window.jQuery || document.write('<script src="./assets/js/vendor/jquery-1.11.1.min.js"><\/script>');
        </script>

        <script src="<?php echo $base_url; ?>assets/js/vendor/jquery.mask.min.js"></script>
        <script src="<?php echo $base_url; ?>assets/js/vendor/owl.carousel.min.js"></script>
        <script src='<?php echo $base_url; ?>assets/js/vendor/fastclick.js'></script>
        <script src='<?php echo $base_url; ?>assets/js/vendor/jquery.lazyload.min.js'></script>
        <script src='<?php echo $base_url; ?>assets/js/vendor/jquery.nicescroll.min.js'></script>
        <script src='<?php echo $base_url; ?>assets/js/vendor/jquery.gvalert.min.js'></script>
        <script src="<?php echo $base_url; ?>assets/js/main.js"></script>
        <script src="<?php echo $base_url; ?>assets/js/home.js"></script>
        <script src="<?php echo $base_url; ?>assets/js/contato.js"></script>
        <script src="<?php echo $base_url; ?>assets/js/servicos.js"></script>
        <script src="<?php echo $base_url; ?>assets/js/sobre.js"></script>
        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-62127754-1', 'auto');
          ga('send', 'pageview');

        </script>
    </body>
</html>
