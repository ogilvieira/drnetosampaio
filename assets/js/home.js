$(document).ready(function(){
    sliderQ = $(".main-slider .item").length;
    if(sliderQ > 1){
        $(".main-slider").owlCarousel({
            autoPlay : true,
            stopOnHover : true,
            slideSpeed : 5000,
            singleItem : true,
            pagination: true,
            navigation: true,
            navigationText: false,
            mouseDrag: true,
            transitionStyle: 'fade'
        });
    }

});