$('.localizacao .info .btn').click(function(){
	$('.localizacao .info').fadeOut();
});

var sendmessage = function( elform, fields ){
	
	var btn = elform.find('button[type=submit]');

	btn.attr("disabled","disabled");

	$.ajax({
		url: utils.urlsite+'includes/send_contact.php',
		type: 'post',
		dataType: 'json',
		data: fields,
	})
	.always(function( response ) {
		btn.removeAttr("disabled");
		if( response.send ) {
			gvalert.sucesso("Mensagem enviada com sucesso!");
			elform.find("input, textarea").val("");
		} else {
			gvalert.aviso("Mensagem não enviada. \nPor favor, tente novamente.");
		}
	});
	
};

$('form#contato').submit(function( event ){
	event.preventDefault();

	var campo = {
		nome : $('input[name=nome]', this),
		email : $('input[name=email]', this),
		telefone : $('input[name=telefone]', this),
		msg : $('textarea[name=msg]', this)
	}

	if(campo.nome.val().length < 4){
		gvalert.erro("Preencha o campo \"Nome\" corretamente.");
		campo.nome.addClass('is-incorrect');
		return false;
	}else {
		campo.nome.removeClass('is-incorrect');
	}

	if(campo.email.val().indexOf('@')==-1 || campo.email.val().indexOf('.')==-1){
		gvalert.erro("Preencha o campo \"E-mail\" corretamente.");
		campo.email.addClass('is-incorrect');
		return false;
	}else {
		campo.email.removeClass('is-incorrect');
	}

	if(campo.telefone.val().length < 14){
		gvalert.erro("Preencha o campo \"Telefone\" corretamente.");
		campo.telefone.addClass('is-incorrect');
		return false;
	}else {
		campo.telefone.removeClass('is-incorrect');
	}

	if(!campo.msg.val().length){
		gvalert.erro("Escreva sua duvida.");
		campo.msg.addClass('is-incorrect');
		return false;
	}else {
		campo.msg.removeClass('is-incorrect');
	}
	
	var fields = {
		nome 	 : campo.nome.val(),
		email 	 : campo.email.val(),
		telefone : campo.telefone.val(),
		msg 	 : campo.msg.val()
	};
	ga('send', 'event', 'link','click', 'Contato realizado');

	sendmessage( $(this), fields );
	return false;
});