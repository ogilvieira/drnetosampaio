Menu = {
	Open : function(){
		$('.main-header .btn-menu .fa').removeClass('fa-bars').addClass('fa-close');
		$('body').append('<div class=\"darkshadow\"></div>');
		$('.fixed-menu').animate({'right': '0'}, 200);
		$('.darkshadow').animate({'opacity' : '.8'}, 200);
	},
	Close : function(){
		$('.main-header .btn-menu .fa').removeClass('fa-close').addClass('fa-bars');
		$('.fixed-menu').animate({'right': '-250px'}, 200);
		$('.darkshadow').animate({'opacity' : '0'}, 200);
		setTimeout( function(){
			$('.darkshadow').remove();
		}, 300);
	}
}

$('.btn-menu').click(function(e){
	e.preventDefault();
	
	if($('.fa',this).hasClass('fa-bars')){
		Menu.Open();

	}else {
		Menu.Close();		
	}
});


$('body').delegate('.darkshadow', 'click', function(){
	Menu.Close();
});

$("img.lazy").lazyload({
    effect : "fadeIn"
});

$(window).resize(function(){
	var w = $(window).width();
	if(w > 980){
		$('.darkshadow').hide();
	}else {
		$('.darkshadow').show();
	}
})

$(document).ready(function() { 
	$("html").niceScroll({cursorcolor:"rgba(0, 0, 0, 0.3)", cursorborder:"none", zindex:"9999999"})
});

if( navigator.userAgent.match(/Android/i)
 || navigator.userAgent.match(/webOS/i)
 || navigator.userAgent.match(/iPhone/i)
 || navigator.userAgent.match(/iPad/i)
 || navigator.userAgent.match(/iPod/i)
 || navigator.userAgent.match(/BlackBerry/i)
 ){
	FastClick.attach(document.body);
}


$('.scroll-to').click(function(e){
	e.preventDefault();
	var t = '#' + $(this).data("target");

	$('body, html').animate({scrollTop: $(t).offset().top-30}, 500);

});