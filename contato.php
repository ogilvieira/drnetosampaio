<?php
$title = 'Entre em contato';
$description = 'Mande uma mensagem! nosso atendimento esta disposto a te orientar e responder todas suas dúvidas sobre os nossos procedimentos dermatológicos e estéticos.';
$keywords = 'duvidas, contato, fale, atendimento, localização, jardim, paulista, são, paulo, telefone, email, como, chegar';
$menu_marker = 'contato';
include './includes/header.php';
?>
<div class="clearfix"></div>
<section class="localizacao">
	<div class="map">
		<iframe src="//www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14628.07344790268!2d-46.657708!3d-23.567784!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x834dab337721d76e!2sDr.+Jos%C3%A9+Sampaio+Lopes+Neto!5e0!3m2!1sen!2sbr!4v1429627666553" width="600" height="450" frameborder="0" style="border:0"></iframe>
	</div>
	<div class="info">
		<div class="address-wrap">
			<div class="icon">
				<i class="fa fa-map-marker"></i>
			</div>
			<h2 class="title">LOCALIZAÇÃO</h2>
			<div class="address">
				Rua Pamplona, 1222<br/>
				Jd. Paulista, São Paulo - SP<br/>
				CEP 01405-001
			</div>
			<button class="btn">
			ver mapa
			</button>
		</div>
	</div>
</section>
<section class="contato">
	
	<div class="container-12">
		<div class="grid-8">
			<div class="form-area">
				<h2>Mande <span>uma mensagem</span>:</h2>
				<form action="" method="post" id="contato">
					<div class="campo">
						<input type="text" name="nome" placeholder="Nome*">
					</div>
					<div class="campo">
						<input type="text" name="email" placeholder="E-mail*">
					</div>
					<div class="campo">
						<input type="text" name="telefone" placeholder="Telefone" data-mask="(00) 0000-00000">
					</div>
					<div class="campo">
						<textarea name="msg" placeholder="Mensagem*" cols="30" rows="10"></textarea>
					</div>
					<div class="campo">
						<span class="label">*campo obrigatório</span>
						<button type="submit">ENVIAR</button>
					</div>
				</form>
			</div>
		</div>
		<div class="grid-4">
			
			<div class="ligue-agora">
				<h2>
				Ligue agora<br/>
				e <span>fale conosco</span>:
				</h2>
				<a href="tel:">(11) 3884-1619</a>
			</div>
			<hr>
			<div class="horario-de-atendimento">
				<strong>Horário de Atendimento</strong>
				Segunda a Sexta<br/>
				9 as 18hs
			</div>
			<hr>
			<div class="parceiro">
				Parceiro:
				<a href="#">
					<img src="<?php echo $base_url; ?>assets/img/parceiros/b1-branqueamento-dental.jpg" alt="">
				</a>
			</div>
		</div>
	</div>
</section>
<div class="clearfix"></div>
<?php include 'includes/footer.php'; ?>