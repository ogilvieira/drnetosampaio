module.exports = function(grunt) {


  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-tinypng');

  // Project configuration.
  grunt.initConfig({

    uglify: {
      dist: {
        src: [
          './assets/js/vendor/jquery.mask.min.js',
          './assets/js/vendor/owl.carousel.min.js',
          './assets/js/main.js'
        ],
        dest: 'assets/js/app.min.js'
      }
    },

    cssmin: {
      dist: {
        src: [
          './assets/css/vendor/normalize.min.css',
          './assets/css/vendor/font-awesome.min.css',
          './assets/css/vendor/owl.carousel.css',
          './assets/css/vendor/owl.transitions.css',
          './assets/css/vendor/grid-1200.css',
          './assets/css/main.css'
        ],
        dest: './assets/css/app.min.css'
      }
    },
    tinypng: {
      options: {
          apiKey: "AUBjqqMZLp_skRMg_Pcgx5hItXXSawZW",
          checkSigs: true,
          summarize: true,
          showProgress: true,
          stopOnImageError: true
      }, 
      compress: {
        expand: true, 
        src: './assets/img/*.png', 
        dest: '/',
        ext: '.png'
      }
    }    
  });


  // Default task(s).
  grunt.registerTask('default', ['uglify', 'cssmin']);

};