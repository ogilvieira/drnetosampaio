<?php
$title = 'Serviços dermatológicos e estéticos';
$description = 'Confira os serviços que oferecemos para te deixar cada vez mais linda. Aproveite e tire suas duvidas.';
$keywords = 'botox, preenchimentos, peeling, peelings, quimicos, jet, colágeno, radiofrequência, mathus, tratamento, péle, lisa, acne, espinhas, rejuvenescimento, gordura, localizada, celulite';
$menu_marker = 'servicos';
include 'includes/header.php';
?>
<div class="clearfix"></div>

<div class="cover-page servicos">
	<h1>Serviços</h1>
</div>

<main class="page servicos">
	<div class="container-12">
		<div class="grid-6">

			<div class="servico-item">
				<div class="figure">
					<div class="image">
						<figure>
							<img class="lazy" data-original="<?php echo $base_url; ?>assets/img/servicos/botox.jpg" alt="" width="580" height="300">
						</figure>
					</div>
					<div class="green-layer">
						<button class="btn-tire-duvidas scroll-to" data-target="tire-suas-duvidas">Tire suas Duvidas</button>
						<span>
							Clique no botão e <br/>nos envie suas perguntas
						</span>
					</div>
				</div>
				<!-- /.figure -->
				<div class="description">
					<h2>Botox</h2>
					<p>
						Por ter um efeito de paralisar a musculatura passou a ser utilizado, para fins estéticos, no tratamento de linhas de expressão dinâmicas, linhas faciais e no tratamento da <strong>hiperidrose</strong> (suor excessivo).<br/>
						<strong>A aplicação de botox é muito segura</strong>, principalmente quando realizada por um dermatologista ou outro médico especializado na sua aplicação.
					</p>
				</div>
			</div>
			<!-- /.servicos-item -->

			<div class="servico-item">
				<div class="figure">
					<div class="image">
						<figure>
							<img class="lazy" data-original="<?php echo $base_url; ?>assets/img/servicos/peeling.jpg" alt="" width="580" height="300">
						</figure>
					</div>
					<div class="green-layer">
						<button class="btn-tire-duvidas scroll-to" data-target="tire-suas-duvidas">Tire suas Duvidas</button>
						<span>
							Clique no botão e <br/>nos envie suas perguntas
						</span>
					</div>
				</div>
				<!-- /.figure -->
				<div class="description">
					<h2>Peelings químicos</h2>
					<p>
						Os efeitos dos ácidos sobre a pele podem aparecer mais rapidamente quando utilizados em alta concentração para a realização dos peelings químicos.<br/><br/>
						 
						Os <strong>peelings</strong> são procedimentos médicos, pois em mãos inábeis podem trazer resultados desastrosos. Nestes procedimentos, podem ser utilizados diversos tipos de ácidos de acordo com o resultado que se deseja obter e com a profundidade que se deseja atingir. Sendo usados para rejuvenescimento, manchas e até acne.<br/><br/>
						 
						<strong>Peeling jet</strong> / Indicações no tratamento de sucos e linhas finas faciais, sequelas de acne, estrias, quelóides, hipermigmentação/desordens pigmentares e revitalização tissular por estimular a produção de colágeno.<br/><br/>
						 
						O <strong>Peeling de cristal</strong> também é indicado como procedimento preliminar e potencializador de outras técnicas de tratamentos, pois, sua aplicação reduz a capa córnea da pele, favorecendo a passagem de correntes, veiculação de ativos de cosméticos e ácidos.
					</p>
				</div>
			</div>
			<!-- /.servicos-item -->
		</div>


		<div class="grid-6">

			<div class="servico-item">
				<div class="figure">
					<div class="image">
						<figure>
							<img class="lazy" data-original="<?php echo $base_url; ?>assets/img/servicos/preenchimentos.jpg" alt="" width="580" height="300">
						</figure>
					</div>
					<div class="green-layer">
						<button class="btn-tire-duvidas scroll-to" data-target="tire-suas-duvidas">Tire suas Duvidas</button>
						<span>
							Clique no botão e <br/>nos envie suas perguntas
						</span>
					</div>
				</div>
				<!-- /.figure -->
				<div class="description">
					<h2>Preenchimentos</h2>
					<p>
						Preenchimentos dérmicos injetáveis são usados para <strong>aumentar lábios finos</strong>, melhorar <strong>contornos superficiais</strong>, suavizar <strong>rugas faciais</strong>, eliminar <strong>rugas</strong> e melhorar a aparência das <strong>cicatrizes</strong>.
					</p>
				</div>
			</div>
			<!-- /.servicos-item -->

			<div class="servico-item">
				<div class="figure">
					<div class="image">
						<figure>
							<img class="lazy" data-original="<?php echo $base_url; ?>assets/img/servicos/lasers.jpg" alt="" width="580" height="300">
						</figure>
					</div>
					<div class="green-layer">
						<button class="btn-tire-duvidas scroll-to" data-target="tire-suas-duvidas">Tire suas Duvidas</button>
						<span>
							Clique no botão e <br/>nos envie suas perguntas
						</span>
					</div>
				</div>
				<!-- /.figure -->
				<div class="description">
					<h2>Radiofrequência</h2>
					<p>
						Hertix objetivo do tratamento é aumentar a temperatura do tecido no sentido de alcançar uma temperatura local de 40 ºC a 43 ºC, o que desencadeia uma sequência de reações fisiológicas: ocorre a produção de um aquecimento na área tratada, <strong>formação de um novo colágeno</strong> (uma das fibras de sustentação e preenchimento da pele) e o <strong>aumento do metabolismo das células gordurosas</strong>.
					</p>
				</div>
			</div>
			<!-- /.servicos-item -->

			<div class="servico-item no-border">
				<div class="figure">
					<div class="image">
						<figure>
							<img class="lazy" data-original="<?php echo $base_url; ?>assets/img/servicos/mesoterapia.jpg" alt="" width="580" height="300">
						</figure>
					</div>
					<div class="green-layer">
						<button class="btn-tire-duvidas scroll-to" data-target="tire-suas-duvidas">Tire suas Duvidas</button>
						<span>
							Clique no botão e <br/>nos envie suas perguntas
						</span>
					</div>
				</div>
				<!-- /.figure -->
				<div class="description">
					<h2>Mathus</h2>
					<p>
						Totalmente indolor, o Manthus é <strong>um dos procedimentos mais eficientes para queimar a gordura localizada</strong> e reduzir a <strong>celulite</strong>, combinando ultrassom e correntes elétricas que quebram as moléculas de gordura e estimulam a ação dos vasos linfáticos, além de promoverem um aumento na produção de colágeno.<br/><br/>
						Além disso, ele também oferece excelentes resultados no pós-operatório e previne aderência e fibroses, com um programa anti-inflamatório específico para essa finalidade.
					</p>
				</div>
			</div>
			<!-- /.servicos-item -->
		</div>

	</div>
</main>

<div id="tire-suas-duvidas"></div>
<section class="tire-suas-duvidas">
	<div class="container-12">
		<div class="grid-12">
			<div class="form-area">
				<h2>Tire suas dúvidas</h2>
				<form action="" method="post" id="duvidas">
					<div class="campo">
						<input type="text" name="nome" placeholder="Nome*">
					</div>
					<div class="campo">
						<input type="text" name="email" placeholder="E-mail*">
					</div>
					<div class="campo">
						<input type="text" name="telefone" placeholder="Telefone" data-mask="(00) 0000-00000">
					</div>
					<div class="campo check-group">
						<span class="title">Sobre quais serviços você quer perguntar?</span>

						<div class="check"><input class="servico" name="servico[]" type="checkbox" id="input-1" value="Botox"> <label for="input-1">	<span>Botox</span></label></div>
						<div class="check"><input class="servico" name="servico[]" type="checkbox" id="input-2" value="Peeling"> <label for="input-2">	<span>Peeling</span></label></div>
						<div class="check"><input class="servico" name="servico[]" type="checkbox" id="input-3" value="Preenchimentos"> <label for="input-3">	<span>Preenchimentos</span></label></div>
						<div class="check"><input class="servico" name="servico[]" type="checkbox" id="input-4" value="Radiofrequência"> <label for="input-4">	<span>Radiofrequência</span></label></div>
						<div class="check"><input class="servico" name="servico[]" type="checkbox" id="input-6" value="Mathus"> <label for="input-6">	<span>Mathus</span></label></div>

					</div>
					<div class="campo">
						<textarea name="msg" id="" placeholder="Escreva aqui sua duvida*" cols="30" rows="10"></textarea>
					</div>
					<div class="campo">
						<span class="label">*campo obrigatório</span>
						<button type="submit">ENVIAR</button>
					</div>
				</form>						
			</div>
		</div>	
	</div>
</section>

<div class="clearfix"></div>

<?php include 'includes/footer.php'; ?>