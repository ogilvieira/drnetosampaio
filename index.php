<?php
    $title = '';
    $description = '';    
    $keywords = '';
    $menu_marker = 'home';

    include 'includes/header.php';
?>
    <div class="clearfix"></div>

    <section class="main-slider">
        <div class="item lazy" data-original="<?php echo $base_url; ?>assets/img/slides/slide-1.jpg" style="background-image:url(<?php echo $base_url; ?>assets/img/slides/slide-1.jpg);">
            <div class="container-12">
                <div class="grid-12">
                    <div class="description green">
                        <h2>Beleza + Saúde</h2>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam erat magna, iaculis id augue sit amet, vestibulum lacinia tortor. Vivamus.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="item lazy" data-original="<?php echo $base_url; ?>assets/img/slides/slide-2.jpg" style="background-image:url(<?php echo $base_url; ?>assets/img/slides/slide-2.jpg);">
            <div class="container-12">
                <div class="grid-12">
                    <div class="description">
                        <h2>Dr. José Sampaio Lopes Neto</h2>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam erat magna, iaculis id augue sit amet, vestibulum lacinia tortor. Vivamus.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <main class="main-content home">
        <div class="container-12">
            <div class="grid-7">
                <a href="<?php echo $base_url; ?>servicos" class="card card-1">
                    <div class="image" style="background-image: url('<?php echo $base_url; ?>assets/img/card/card-1.jpg');"></div>
                    <div class="description">
                        <div class="text">
                            <h2>PEELINGS QUÍMICOS</h2>
                            <p>
                                Sua pele lisinha, sem manchas, acne ou cicatrizes. Um peeling sequencial intensivo que irá surpreender você! Comprove!
                            </p>
                        </div>
                        <div class="saiba-mais">
                            <span class="btn">Saiba Mais</span>
                        </div>
                    </div>
                </a>
            </div>
            <!-- .grid -->

            <div class="grid-5">
                <a href="<?php echo $base_url; ?>servicos" class="card card-2">
                    <div class="image" style="background-image: url('<?php echo $base_url; ?>assets/img/card/card-2.jpg');"></div>
                    <div class="description">
                        <div class="text">
                            <h2>Botox</h2>
                            <p>
                                Pensando em se livrar das rugas com botox? Conheça tudo sobre o botox e tire suas dúvidas.
                            </p>
                        </div>
                        <div class="saiba-mais">
                            <span class="btn">Saiba Mais</span>
                        </div>
                    </div>
                </a>
            </div>
            <!-- .grid -->

            <div class="grid-7">
                <a href="<?php echo $base_url; ?>contato" class="card card-3">
                    <div class="image" style="background-image: url('<?php echo $base_url; ?>assets/img/card/card-3.jpg');"></div>

                    <div class="description">
                        <div class="text">
                            <h2>Contato</h2>
                            <p>
                                Marque uma consulta ou tire dúvidas agora mesmo.
                            </p>
                        </div>
                        <div class="saiba-mais">
                            <span class="btn">Saiba Mais</span>
                        </div>
                    </div>

                </a>
            </div>
            <!-- .grid -->

        </div>
        <!-- /.container -->
    </main>

    <div class="clearfix"></div>

    <?php include 'includes/footer.php'; ?>