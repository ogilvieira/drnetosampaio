<?php
    $title = 'Sobre';
    $description = '';    
    $keywords = '';
    $menu_marker = 'sobre';

    include 'includes/header.php';
?>
<section class="cover-profile">
	<div class="container-12">
		<div class="grid-12">
			<div class="profile">
				<figure>
					<img src="<?php echo $base_url; ?>assets/img/dr-jose-sampaio-lopes-neto.jpg" alt="" width="278" height="278">
				</figure>
				<figcaption>
					<h1>JOSÉ SAMPAIO LOPES NETO</h1>
					<p>Dermatologia & Estética Médica</p>
				</figcaption>
			</div>
		</div>
	</div>
</section>

<div class="about-control">
	<div class="container-12">
		<div class="grid-12">
			<a class="btn is-active" data-target="timeline" href="#">Linha do tempo</a>
			<a class="btn" data-target="article" href="#">História</a>
		</div>
	</div>
</div>

<main class="page-sobre">
	<div class="container-12">
		<section class="timeline is-active target-tab">
			<div class="grid-5">
				
				<div class="item">
					<span class="year">
						2003
					</span>
					<div class="content">
						<div class="text">
							<header>
								<h2> <i class="fa fa-mortar-board"></i>Estudou Odontologia </h2>
							</header>
							<p>
								Universidade de São Paulo (UNICID) <br/>São Paulo, SP 

							</p>
						</div>
						<figure class="cover">
							<img src="<?php echo $base_url; ?>assets/img/timeline/estudou-odontologia.jpg" alt="Estudou Odontologia" width="556" height="203">
						</figure>
					</div>
				</div>


				<div class="item">
					<span class="year">
						2009
					</span>
					<div class="content">
						<div class="text">
							<header>
								<h2> <i class="fa fa-mortar-board"></i>Formou-se em medicina</h2>
							</header>
							<p>
								Universidade Metropolitana de Santos (UNIMES) <br/>Santos, SP 
							</p>
						</div>
						<figure class="cover">
							<img src="<?php echo $base_url; ?>assets/img/timeline/formou-se-em-medicina.jpg" alt="Formou-se em medicina" width="556" height="203">
						</figure>
					</div>
				</div>


				<div class="item">
					<span class="year">
						2009
					</span>
					<div class="content">
						<div class="text">
							<header>
								<h2> <i class="fa fa-heartbeat"></i>Aprovado no curso ACLS</h2>
							</header>
							<p>
								Advance Cardiologic Life Support ministrado no Hospital Sírio Libanês
							</p>
						</div>
						<figure class="cover">
							<img src="<?php echo $base_url; ?>assets/img/timeline/aprovado-no-curso-acls.jpg" alt="Aprovado no curso ACLS" width="556" height="203">
						</figure>
					</div>
				</div>

				<div class="item">
					<span class="year">
						2015
					</span>
					<div class="content">
						<div class="text">
							<header>
								<h2> <i class="fa fa-bookmark"></i>Certificado pela AAAM</h2>
							</header>
							<p>
								American Academy Of Aesthetic Medicine
							</p>
						</div>
						<figure class="cover">
							<img src="<?php echo $base_url; ?>assets/img/timeline/aaam.jpg" alt="Certificado pela AAAM" width="556" height="203">
						</figure>
					</div>
				</div>												

			</div>
		</section>
		<!-- /.timeline -->
		<!-- /.grid -->
		<div class="grid-7">
			<article class="article target-tab">
				<header>
					<h1>Titulo do texto</h1>
				</header>
				<p>Lorem ipsum dolor sit amet, <strong>consectetur</strong> adipisicing elit. Voluptatem possimus
					voluptates architecto tempore debitis officia corporis magni sequi qui vero
					accusantium saepe unde laboriosam vitae labore ducimus ad, quidem consectetur culpa atque beatae at, laborum eligendi. Rerum inventore, numquam ea iure dicta at, optio, libero eligendi laboriosam quia minima incidunt nam blanditiis reiciendis deserunt eos, reprehenderit.
				</p>
				<p>
					Sint praesentium <em>consequatur</em>, eius <a href="#">voluptatum</a>! Ullam adipisci provident nostrum pariatur aliquid quaerat, nam numquam voluptatem natus doloribus! Culpa veritatis a, deleniti distinctio incidunt cupiditate ipsa dolorem nobis provident eaque, in non natus fuga, odio, modi laudantium commodi sit aliquid magni dolor possimus quasi. Vitae, quo iure inventore accusamus animi debitis repudiandae temporibus vero! Rerum, est labore voluptatibus repellat cupiditate dolorum ullam nulla, dolores iste, suscipit eum nam dolor dolorem ipsum id rem magni asperiores sapiente animi dicta.
				</p>
				<h2>
				Subtitulo
				</h2>
				<p>
					Modi labore minima, quaerat quibusdam! Ducimus dolor, reprehenderit tenetur sequi voluptate beatae, quis. Dignissimos porro quas magni itaque expedita libero laboriosam sequi officia, molestias nobis, animi voluptatem nam labore suscipit
					asperiores culpa vitae assumenda quo officiis hic quis fuga voluptas, aliquid fugit quasi! Qui voluptates dolorem obcaecati suscipit ipsam temporibus iure officiis ipsum velit, commodi quos officia sed, excepturi quas maxime corporis eius eveniet itaque necessitatibus dignissimos explicabo vel exercitationem quisquam tempore. Optio architecto rem consequuntur magni iste fugiat nesciunt quod impedit ipsam libero sed dolorem, nihil amet nam voluptatum similique tempora at sunt,
					assumenda.
				</p>
				<p>
					Dolore ratione delectus, obcaecati atque explicabo quia minus enim provident
					recusandae fuga veniam, laboriosam error expedita architecto? Vel iste unde quia impedit rerum, dolor maxime ipsam ipsa. Saepe magnam tempora, quo
					doloremque praesentium maiores aliquam similique consequatur recusandae labore placeat fuga officiis officia quia.
				</p>
				<img src="http://dummyimage.com/680x300/ccc/444.jpg" alt="">
				<p>
					Quaerat nihil omnis earum tenetur officia repellat obcaecati voluptatem ipsum ex rem recusandae vero, assumenda quidem. Ipsam repellendus modi explicabo est quo voluptates animi dolorum obcaecati! Odit culpa sequi nihil id odio velit ex vel, tempore, esse a dicta atque officia fugiat voluptates iusto accusamus ipsam ut?
				</p>
				<p>
					Fugiat dignissimos illum eveniet adipisci repellat at, dolorum doloribus.
					Exercitationem nam inventore reprehenderit voluptatem, adipisci officiis quibusdam tenetur eligendi eaque temporibus esse modi sed, iste iusto laudantium officia omnis distinctio, nisi ducimus. Aliquid, molestiae! Delectus doloremque at rerum consequatur nihil modi sed necessitatibus tenetur optio ipsum. Dicta vero corrupti, quidem praesentium alias, optio. Illum reiciendis nulla architecto obcaecati, impedit commodi ut tenetur quod magni dolores provident eaque optio modi. Et autem
					incidunt necessitatibus repellendus, perspiciatis, eveniet fuga aliquid sit pariatur
					placeat?
				</p>
			</article>
		</div>
	</div>
	
</main>

    <?php include 'includes/footer.php'; ?>